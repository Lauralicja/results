##### Sun, 19 Apr 2020 10:34:36 +0200
# fox-geese-corn
|Group|Status|Performance|Competition|
|----:|:-----|:----------|:----------|
|[Logically Querulous](https://gitlab.com/logically-querulous/fox-geese-corn)|20%| N/A| N/A|
|[Zaratusztrianie](https://gitlab.com/zaratusztrianie/fox-geese-corn)|100%| 33% - 102 (6.45s)| N/A|
|[sealmove]()| | |FAIL: Incorrect project settings: project should be private|
|[teacher](https://gitlab.com/agh-krr/teacher/fox-geese-corn)|100%| 33% - 102 (0.45s)| 910 (2020-04-11 16:23:47 +0200)|
---
# production-planning
|Group|Status|Performance|Competition|
|----:|:-----|:----------|:----------|
|[teacher](https://gitlab.com/agh-krr/teacher/production-planning)|100%| 66% - 1185 (10.12s)| 25755 (2020-04-11 16:33:51 +0200)|
---
# gangs-wars
|Group|Status|Performance|Competition|
|----:|:-----|:----------|:----------|
|[cool names are cool](https://gitlab.com/cool-names-are-cool/gangs-wars)|25%| N/A| N/A|
|[krr-project-mt](https://gitlab.com/krr-project-mt/gangs-wars)|100%| 66% - 37 (8.01s)| 207 (2020-04-19 01:04:32 +0000)|
|[teacher](https://gitlab.com/agh-krr/teacher/gangs-wars)|100%| 100% - 38 (8.00s)| 207 (2020-04-11 16:24:05 +0200)|
---
# job-fair
|Group|Status|Performance|Competition|
|----:|:-----|:----------|:----------|
|[artur minh](https://gitlab.com/artur.minh/job-fair)|25%| N/A| N/A|
|[Alpacacorns](https://gitlab.com/alpacacorns/job-fair)|25%| N/A| 7380 (2020-04-18 19:53:00 +0200)|
|[Eternal Sun Inquisition](https://gitlab.com/eternal-sun-inquisition/job-fair)|25%| N/A| N/A|
|[Filthy Casuals](https://gitlab.com/filthy-casuals/job-fair)|| N/A| N/A|
|[teacher](https://gitlab.com/agh-krr/teacher/job-fair)|100%| 66% - 2815 (31.47s)| 3654 (2020-04-11 15:19:54 +0200)|
---
